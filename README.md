# Pljeskavica's Network Monitor

Feature list:

 * Run Continuous MTR's
 * Configurable Destination
 * Save with Project name prefix
 * Save with current Date and Time
 * Get output in window for quick visibility



Recommended Instructions:
 * save network-monitor.sh in your user folder with a . infront of it to make it hidden
 * open bash_profile and add an alias:
 ```bash
alias networkMonitor="~/.network-monitor.sh"
 ```
 * Change permissions of script to be executable:
```bash
chmod +x ~/.network-monitor.sh
```



Starting the Tool:

To run the tool you need to supply two arguments.  The first is the project/file prefix name.  The second is the network location that you would like to monitor access to.  For best results you should try to use an ip address in the same data center as the service you are actually using.  

Example:

```bash
networkMonitor google 8.8.8.8
```