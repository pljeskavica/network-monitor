#!/bin/sh

echo "Initializing X's network monitor...."
echo "------------------"
if [ $# != 2 ]
  then
    echo "Err: Arguments Mismatch"
    echo "Please submit the following arguments:"
    echo "1: Show name which is used as the file prefix"
    echo "2: Network location that you want to monitor access to"
    echo "Example:"
    echo "watchNetwork Clash 8.8.8.8"
    exit
fi

COUNTER=1
while true
do 

    file="$1_mtr_$(date '+%Y-%m-%d_%H-%M').txt"
    # file="$1_mtr_$(date '+%H-%M').txt"
    echo "Starting MTR wave $COUNTER"
    sudo mtr --report --report-cycles 100 $2 >"$file"
    echo "MTR wave $COUNTER complete, results:"
    echo "------------------"
    more "$file"
    echo "------------------"
    let COUNTER=COUNTER+1
    sleep 60
done
